# Laravel Vite Nord Icons

This repository reproduces the issue where importing two or more components with
the same Nord Icon will result in only the first Nord icon to be rendered.

For more details, please take a look at the dedicated Slack conversation.

## How to get started

There are two ways to get started with this repository: either using a local
installation of PHP, or by using [Laravel Sail](https://laravel.com/docs/9.x/sail#main-content).

I will guide you through using Sail as it's a docker configuration that should
have everything you need to get the app starting

1. Rename (or copy) `.env.example` to `.env`
2. Install the frontend dependencies using `pnpm install` (Node v18.7.0)
3. Build the frontend using `pnpm build`
4. Install the PHP dependencies using:
   ```
   docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
   ```
5. Start Laravel Sail with `./vendor/bin/sail up -d`
6. That should be it! Visit http://localhost.

Important: if you wish to use the development version of Vite, start it with the
normal `pnpm dev` command, but afterwords **visit http://localhost as always**.
